
local function leChiffreEstBon(chiffre) 
  return 0 < chiffre and chiffre < 100 
end


function all_trim(str)
  return string.gsub(str, "%s+", "")
end

function demandeNombreEtAffiche(message)
  print(message)
  local chiffre = io.read("*number")
   if chiffre and leChiffreEstBon(chiffre) then 
    return chiffre
  else
    return demandeNombreEtAffiche("Ce n'est pas un nombre valide , Re-essayez svp")
  end
end



function demandeCharMajEtAffiche(message)
  print(message)
  local chaine = io.read()
   if chaine then 
     return string.upper(all_trim(chaine))
   else
     return demandeCharMajEtAffiche("")
   end
end
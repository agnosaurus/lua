require("utils.inputClavier");


local function initMask(mot)
  result =""
  for i =1, #mot do
    result=result.."_"
  end

return result
end

function replace_char(pos, str, r)
    return str:sub(1, pos-1) .. r .. str:sub(pos+1)
end

-- NE PAS MODIFIER
local function updateMasque(mot, masque, lettre)
newMasque =""
nouvelleLettretrouve=false

for i = 1, #mot do
  local lettreMot = string.upper(mot:sub(i,i))
  local lettreMasque = masque:sub(i,i)
    
  if (lettreMasque == "_") then
    if lettre == lettreMot then
      newMasque=newMasque..lettreMot
      nouvelleLettretrouve=true
    else
      newMasque=newMasque.."_"
    end
      
  else
    newMasque=newMasque..lettreMasque
  end   
end

if nouvelleLettretrouve == false then
  nbError=nbError+1
end



return newMasque
end



-- Definition de la fonction main = fonction principale du programme
local function derouleJeu(aDeviner, maxErrors)

  masque = initMask(aDeviner)
--for i = 1, 45 do
--   print()
--end

io.read()
nbError = 0

while string.find(masque,"_") ~= nil and nbError < maxErrors do
  lastInput = demandeCharMajEtAffiche("Choisis une lettre "..masque)
  masque = updateMasque(aDeviner, masque,lastInput)
end

print(masque)


if string.find(masque,"_") ~= nil  then
  print("Tu as fait trop d'erreurs !")
else
  print("GG  en "..nbError .."/"..maxErrors .." erreurs permises")
end
end

local function main()


-- On demande le mot
aDeviner = demandeCharMajEtAffiche("Choisis le mot  a deviner !")

-- On demande le nb max d essais
maxErrors =  demandeNombreEtAffiche("Choisis ton nombre max d'erreurs")

-- On deroule le jeu
derouleJeu(aDeviner,maxErrors)
  
end
-- Appel method main
main()

-- la doc LUA en francais https://wxlua.developpez.com/tutoriels/lua/general/cours-complet/ ( pas simple � comprendre.... )




-- Meta class
 Voiture = {info = ""}

function Voiture:new (o,marque,annee,prix)
  o = o or {}
  setmetatable(o,self)
   self.__index = self
   marque = marque or 0
   annee = annee or 0
   prix = prix or 0
   self.info= marque.." // "..annee .." // "..prix
   return o
end


 function Voiture:printInfo ()
    print("Info sur cette voiture : ",self.info)
 end


-- Explication sur l objet
voiture1 = Voiture:new(nil,"Renault",2015,8000)
voiture1:printInfo()



voiture2 = Voiture:new(nil,"Mini",2020,18000)
voiture2:printInfo()


-- Explication rapide sur les tables
-- color  = {"RED","BLUE"}
-- print(color[1])
-- print( color[2])
 
 
 
 -- Table = tableau associatif
-- voitureTable = {marque = "Renault",annee = 2015,prix = 8000}
-- voitureTable:printInfo()

 
 -- Explication tableau d objet
-- stock  = {voiture1, voiture2}
 -- stock[1]:printInfo()
-- stock[2]:printInfo()